<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Author */

$this->title = Yii::t('admin', 'Create Author');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Authors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
