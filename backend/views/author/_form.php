<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Author */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="author-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'age')->widget(\kartik\select2\Select2::class, [
            'data' => range(0, 120),
            'language' => Yii::$app->language,
            'options' => ['placeholder' => Yii::t('admin', 'Select age')],
            'pluginOptions' => [
                'allowClear' => true
            ],

        ]) ?>

        <?= $form->field($model, 'country')->dropDownList(\common\models\Author::getCountries())?>
        <?= $form->field($model, 'status')->dropDownList(\common\models\Author::statusList()) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
