<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $book common\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($book, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($book, 'pages')->widget(\kartik\select2\Select2::class, [
            'data' => range(0, 1500),
            'options' => ['placeholder' => Yii::t('admin', 'Select number of pages')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
        <?= $form->field($book, 'year')->widget(\kartik\select2\Select2::class, [
            'data' => range(0, date('Y')),
            'options' => ['placeholder' => Yii::t('admin', 'Select year')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

        <?= $form->field($book, 'genre')->textInput(['maxlength' => true]) ?>

        <?= $form->field($book, 'author_id')->widget(\kartik\select2\Select2::class, [
            'data' => $data,
            'language' => Yii::$app->language,
            'options' => ['placeholder' => Yii::t('admin', 'Select an author')],
            'pluginOptions' => [
                    'default',
                'allowClear' => true
            ],
        ]); ?>
        <?= $form->field($book, 'status')->dropDownList(\common\models\Book::statusList()) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
