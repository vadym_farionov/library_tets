<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $book common\models\Book */
/* @var $author common\models\Author[] */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="book-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($book, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($book, 'pages')->textInput();?>
        <?= $form->field($book, 'year')->textInput();?>

        <?= $form->field($book, 'genre')->textInput(['maxlength' => true]) ?>

        <?= $form->field($book, 'author_id')->dropDownList(ArrayHelper::map($author, 'id','name')) ?>
        <?= $form->field($book, 'status')->dropDownList(\common\models\Book::statusList()) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
