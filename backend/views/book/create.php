<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $book common\models\Book */

$this->title = Yii::t('admin', 'Create Book');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Books'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-create">
    <? if (!Yii::$app->session->hasFlash('no authors')): ?>
        <?= $this->render('_form', [
            'data' => $data,
            'book' => $book,
        ]) ?>
    <? else: ?>
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <?= Yii::$app->session->getFlash('no authors') ?><?php echo Yii::$app->session->getFlash('success'); ?>
        </div>

        <div class="box-header with-border">
            <?= Html::a(Yii::t('admin', 'Create Author'), ['author/create'], ['class' => 'btn btn-success btn-flat']) ?>
        </div>
    <? endif; ?>

</div>
