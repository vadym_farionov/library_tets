<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $book common\models\Book */
/* @var $author common\models\Author */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
    'modelClass' => 'Book',
]) . $book->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Books'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $book->name, 'url' => ['view', 'id' => $book->id]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>
<div class="book-update">

    <?= $this->render('_form_update', [
        'book' => $book,
        'author' => $author,
    ]) ?>

</div>
