<?php

namespace backend\controllers;

use common\models\Author;
use Yii;
use common\models\Book;
use common\models\search\BookSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends SiteController
{

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $book = new Book();
        $authors = ArrayHelper::map(Book::getAuthorsList(), 'id', 'name');
        if ($this->isAuthors()) {
//            var_dump($book->load(Yii::$app->request->post()));
            if ($book->load(Yii::$app->request->post()) && $book->save()) {
                return $this->redirect(['view', 'id' => $book->id]);
            } else {
                return $this->render('create', [
                    'data' => $authors,
                    'book' => $book,
                ]);
            }
        }
        Yii::$app->session->setFlash('no authors', 'You must create author before book.');
        return $this->render('create', [
            'data' => $authors,
            'book' => $book,
        ]);



    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $book = $this->findModel($id);
        $author = Book::getAuthorsList();

        if ($book->load(Yii::$app->request->post()) && $book->save()) {
            return $this->redirect(['view', 'id' => $book->id]);
        } else {
            return $this->render('update', [
                'book' => $book,
                'author' => $author,
            ]);
        }
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function isAuthors()
    {
        $authors = Book::getAuthorsList();
        if ($authors) {
            return true;
        }
        return false;
    }
}
