<?php

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \common\models\Author */

$this->title = 'Authors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <?= Yii::t('library', 'Authors List') ?>
                    </div>
                    <div class="panel-body">
                        <ul class="list-group">
                            <?= \yii\grid\GridView::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    'name',
                                    'surname',
                                    'age',
                                    [
                                        'label' => Yii::t('admin', 'Country'),
                                        'value' => function ($model) {
                                            return $model->getCountry($model->country);
                                        }
                                    ],
                                    [
                                        'format' => 'raw',
                                        'label' => Yii::t('admin', 'Books'),
                                        'value' => function ($model) {
                                            $books = '';
                                            foreach ($model->books as $book) {
                                                $books .= $book->name . "<br>";
                                            };
                                            return $books;

                                        }
                                    ],
                                ],

                            ]) ?>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
