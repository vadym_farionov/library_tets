<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%author}}`.
 */
class m191122_074825_create_author_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%author}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull(),
            'surname' => $this->string(150)->unique()->notNull(),
            'age' => $this->integer(),
            'country' => $this->string(150),
            'status' => $this->integer(1)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%author}}');
    }
}
