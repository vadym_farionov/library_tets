<?php
namespace common\traits;

use common\interfaces\StatusInterface;

trait StatusTrait
{
    /**
     * @return array
     */
    public static function statusList()
    {
        return [
            StatusInterface::STATUS_ACTIVE => \Yii::t('admin', 'Active'),
            StatusInterface::STATUS_INACTIVE => \Yii::t('admin', 'Inactive'),
        ];
    }

    /**
     * @return int
     */
    public static function getActiveStatus()
    {
        return StatusInterface::STATUS_ACTIVE;
    }


    /**
     * @return int
     */
    public static function getInActiveStatus()
    {
        return StatusInterface::STATUS_ACTIVE;
    }

}