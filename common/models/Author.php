<?php

namespace common\models;
use common\traits\StatusTrait;
use common\traits\CountryTrait;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;

/**
 * This is the model class for table "author".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property int|null $age
 * @property string|null $country
 *
 * @property Book[] $books
 */
class Author extends \yii\db\ActiveRecord
{
    use StatusTrait;
    use CountryTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'author';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'status'], 'required'],
            [['age', 'status'], 'integer'],
            [['name', 'surname', 'country'], 'string', 'max' => 150],
            [['surname'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('admin', 'Name'),
            'surname' => Yii::t('admin', 'Surname'),
            'age' => Yii::t('admin', 'Age'),
            'country' => Yii::t('admin', 'Country'),
            'status' => Yii::t('admin', 'Status'),
        ];
    }

    public function getCountry($key)
    {
        $country = Author::getCountries();
        if(array_key_exists($key, $country)){

            return $country[$key];
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Book::class, ['author_id' => 'id']);
    }


    public function getBooksByAuthor($id)
    {
        $author = Author::findOne($id);
        return $author->books;
    }
}
