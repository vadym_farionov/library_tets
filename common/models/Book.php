<?php

namespace common\models;
use common\traits\StatusTrait;
use Yii;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string $name
 * @property int $year
 * @property int $pages
 * @property string $genre
 * @property int $author_id
 *
 * @property Author $author
 */
class Book extends \yii\db\ActiveRecord
{
    use StatusTrait;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'year', 'pages', 'genre', 'author_id'], 'required'],
            [['year', 'pages', 'author_id'], 'integer'],
            [['name', 'genre'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Author::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('admin', 'Name'),
            'year' => Yii::t('admin', 'Year'),
            'pages' => Yii::t('admin', 'Pages'),
            'genre' => Yii::t('admin', 'Genre'),
            'author_id' => Yii::t('admin', 'Author ID'),
        ];
    }

    public static function getAuthorsList()
    {
        return $authors = Author::findAll(['status' => Author::getActiveStatus()]);
    }

    public static function getAuthorName($id)
    {
        return Author::findOne(['id'=> $id])->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::class, ['id' => 'author_id']);
    }
}
